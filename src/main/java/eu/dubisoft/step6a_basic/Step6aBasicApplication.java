package eu.dubisoft.step6a_basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Step6aBasicApplication {

  public static void main(String[] args) {
    SpringApplication.run(Step6aBasicApplication.class, args);
  }

}
